from django.urls import path
from .views import home, mapa, login, contacto, usuarios, registro, cliente, funcionario, administracion

urlpatterns = [
    path('', home, name = "home"),
    path('mapa/', mapa, name = "mapa"),
    path('login/', login, name = "login"),
    path('contacto/', contacto, name = "contacto"),
    path('login/', login, name = "login"),
    path('cliente/', cliente, name = "cliente"),
    path('funcionario/', funcionario, name = "funcionario"),
    path('administracion/', administracion, name = "administracion"), 
    path('registro/', registro, name = "registro"),

]