from django.shortcuts import render

def home(request):
    return render(request, 'MoBike/home.html')

def mapa(request):
    return render(request, 'MoBike/mapa.html')

def contacto(request):
    return render(request, 'MoBike/contacto.html')

def registro(request):
    return render(request, 'Account/registro.html')

def usuarios(request):
    return render(request, 'Mobike/usuarios.html')

def login(request):
    return render(request, 'Account/login.html')  

def administracion(request):
    return render(request, 'Account/administracion.html')

def cliente(request):
    return render(request, 'Account/cliente.html')

def funcionario(request):
    return render(request, 'Account/funcionario.html')




