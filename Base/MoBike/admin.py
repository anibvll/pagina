from django.contrib import admin
from .models import Banco, Cliente, Tarjeta, Usuario, TipoUsuario, Modelo, Marca, Bicicleta, Tramo, Carrera, Boleta 

# Register your models here.

admin.site.register(Banco)
admin.site.register(Cliente)
admin.site.register(Tarjeta)
admin.site.register(TipoUsuario)
admin.site.register(Modelo)
admin.site.register(Marca)
admin.site.register(Bicicleta)
admin.site.register(Tramo)
admin.site.register(Carrera)
admin.site.register(Boleta)
admin.site.register(Usuario)



