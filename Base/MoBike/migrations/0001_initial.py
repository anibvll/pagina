# Generated by Django 3.1.4 on 2020-12-03 02:30

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Banco',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombreBanco', models.CharField(max_length=20)),
            ],
        ),
        migrations.CreateModel(
            name='Cliente',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombreCli', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='Modelo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombreModelo', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='TipoUsuario',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombreTipo', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='Tramo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('distancia', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Usuario',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombreUs', models.CharField(max_length=50)),
                ('contrasenia', models.CharField(max_length=50)),
                ('cliente', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='MoBike.cliente')),
                ('tipoUsuario', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='MoBike.tipousuario')),
            ],
        ),
        migrations.CreateModel(
            name='Tarjeta',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('propietario', models.CharField(max_length=50)),
                ('codSeg', models.IntegerField()),
                ('fechaVen', models.DateField()),
                ('banco', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='MoBike.banco')),
            ],
        ),
        migrations.CreateModel(
            name='Marca',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombreMarca', models.CharField(max_length=50)),
                ('modelo', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='MoBike.modelo')),
            ],
        ),
        migrations.AddField(
            model_name='cliente',
            name='tarjeta',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='MoBike.tarjeta'),
        ),
        migrations.CreateModel(
            name='Carrera',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fechaCarrera', models.DateField()),
                ('inicio', models.CharField(max_length=50)),
                ('final', models.CharField(max_length=50)),
                ('cliente', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='MoBike.cliente')),
            ],
        ),
        migrations.CreateModel(
            name='Boleta',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('monto', models.IntegerField()),
                ('carrera', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='MoBike.carrera')),
            ],
        ),
        migrations.CreateModel(
            name='Bicicleta',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombreBicicleta', models.CharField(max_length=50)),
                ('marca', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='MoBike.marca')),
            ],
        ),
    ]
