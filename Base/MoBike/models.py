from django.db import models

class Banco(models.Model):
    nombreBanco = models.CharField(max_length=20)

    def __str__(self):
        return self.nombreBanco

class Cliente(models.Model):
    nombreCli = models.CharField(max_length=50)
    tarjeta = models.ForeignKey('Tarjeta', on_delete=models.PROTECT)

    def __str__(self):
        return self.nombreCli

class Tarjeta(models.Model):
    propietario = models.CharField(max_length=50)
    codSeg = models.IntegerField()
    fechaVen = models.DateField()
    banco = models.ForeignKey('Banco', on_delete=models.PROTECT)

    def __str__(self):
        return self.propietario

class Usuario(models.Model):
    nombreUs = models.CharField(max_length=50)
    contrasenia = models.CharField(max_length=50)
    tipoUsuario = models.ForeignKey('tipoUsuario', on_delete=models.PROTECT)
    cliente = models.ForeignKey('Cliente', on_delete=models.PROTECT)

    def __str__(self):
        return self.nombreUs

class TipoUsuario(models.Model):
    nombreTipo = models.CharField(max_length=50)

    def __str__(self):
        return self.nombreTipo

class Modelo(models.Model):
    nombreModelo = models.CharField(max_length=50)

    def __str__(self):
        return self.nombreModelo

class Marca(models.Model):
    nombreMarca = models.CharField(max_length=50)
    modelo = models.ForeignKey('Modelo', on_delete=models.PROTECT)

    def __str__(self):
        return self.nombreMarca

class Bicicleta(models.Model):
    nombreBicicleta = models.CharField(max_length=50)
    marca = models.ForeignKey('Marca', on_delete=models.PROTECT)

    def __str__(self):
        return self.nombreBicicleta

class Tramo(models.Model):
    distancia = models.IntegerField()

    def __str__(self):
        return self.distancia

class Carrera(models.Model):
    fechaCarrera = models.DateField()
    inicio = models.CharField(max_length=50)
    final = models.CharField(max_length=50)
    cliente = models.ForeignKey('Cliente', on_delete=models.PROTECT)

    def __str__(self):
        return self.inicio

class Boleta(models.Model):
    monto = models.IntegerField()
    carrera = models.ForeignKey('Carrera', on_delete=models.PROTECT)
    nombreBoleta = models.CharField(max_length=20)


    def __str__(self):
        return self.nombreBoleta





