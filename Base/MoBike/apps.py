from django.apps import AppConfig


class MobikeConfig(AppConfig):
    name = 'MoBike'
